package jcisucursalvirtual;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.amazonaws.services.lambda.runtime.Context;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.lang.Long;
/**
 *
 * @author juanchivision
 */
public abstract class SucursalVirtual implements RequestStreamHandler{

    /**
     * @param args the command line arguments
     */
    
    
    public static void handler(InputStream inputStream, OutputStream outputStream, Context context)
    throws IOException, SQLException, ClassNotFoundException, ParseException {
        String input = toString(inputStream);
        //System.out.println("InputStr: "+input);
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = (JSONObject) parser.parse(input);
        //System.out.println("InputJSON: "+jsonObject);
        JSONObject params = (JSONObject) jsonObject.get("params");
        JSONObject contextJson = (JSONObject) jsonObject.get("context");
        JSONObject bodyJson = (JSONObject) jsonObject.get("body-json");
        JSONObject parameters = (JSONObject) bodyJson.get("parameters");
        //System.out.println("body-json: "+bodyJson);
        System.out.println("parameters: "+parameters);
        JSONObject infoDB = getDBInfo(contextJson);
        //System.out.println("Params: "+params);
        //JSONObject querystring = (JSONObject) params.get("querystring");
        //System.out.println("Querystring: "+querystring);
        String orden_id = (String) parameters.get("id");
        String trx = (String) parameters.get("sucursal_virtual_transaction");
        generarPedido(infoDB, orden_id, trx);
        String response = "{\"status\": 1, \"message\": \"OK\"}";
        outputStream.write(response.getBytes(Charset.forName("UTF-8")));        
    }
    
    public static String toString(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String read;
        while ((read = br.readLine()) != null) {
                sb.append(read);
        }
        br.close();
        return sb.toString();
    }

    public static JSONObject getDBInfo(JSONObject contextJson) throws ParseException {
        String principalID = (String) contextJson.get("authorizer-principal-id");
        String jsonPrincipalID = Base64Coder.decodeString(principalID);
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = (JSONObject) parser.parse(jsonPrincipalID);
        JSONObject database = (JSONObject) jsonObject.get("database");
        System.out.println("database: "+database);
        return database;
    }
    
    /**
     *
     * @param urlDB
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void generarPedido(JSONObject infoDB, String orden_id, String trx) throws IOException, SQLException, ClassNotFoundException, ParseException {
        // TODO code application logic here
        Connection con = getConnection(infoDB);
	    //System.out.println("Conexión generada.");
        if (con != null) {
            //System.out.println("Obteniendo despachos, empresas, receptores y emisores");
            //if (orden_id != null) {
                JSONObject orden = ObtenerOrden(con, orden_id);
                System.out.println("ID: "+orden_id);
                System.out.println("Transaction: "+trx);
                System.out.println("Orden: "+orden);
                //System.out.println("Cerrando conexión de BD.");
            //}
            con.close();
        }
    }
    
    private static Connection getConnection(JSONObject infoDB) throws SQLException, ClassNotFoundException {
        try {
            System.out.println("Creando conexión con BD.");
            Class.forName("org.postgresql.Driver");
            String userName = (String) infoDB.get("user");
            String password = (String) infoDB.get("password");
            String dbName = (String) infoDB.get("database");
            String port = (String) infoDB.get("port");
            String urlDB = (String) infoDB.get("host");
            String rdbms = "postgresql";

            System.out.println("String conexión BD: "+"jdbc:" + rdbms + "://" + urlDB + ":" + port + "/" + dbName);
            
            Connection connection = DriverManager.getConnection("jdbc:" + rdbms + "://" + urlDB + ":" + port + "/" + dbName, userName, password);
            System.out.println("Conexión creada con éxito.");
            return connection;
        }
        catch (ClassNotFoundException e) { System.out.println("Exception: "+e.toString()); e.toString();}
        catch (SQLException e) { System.out.println("Exception: "+e.toString()); e.toString();}
        return null;
    }
    
    public static JSONObject ObtenerOrden(Connection con, String orden_id) throws SQLException, ParseException {
        //System.out.println("Obteniendo emisores.");
        ResultSet rs = null;
        Statement stmt = null;
        JSONObject result = null;
        String query = "select ord_get_order_as_json('"+orden_id+"')";
        String tmp = null;
        int count = 0;
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                tmp = rs.getString(1);
                JSONParser parser = new JSONParser();
                result = (JSONObject) parser.parse(tmp);
                count++;
            }
            //System.out.println(rfc+" Emisores obtenidos: "+count);
            return result;
        } catch (SQLException e ) {
            System.out.println("Exception: "+e.toString());
            e.toString();
        } finally {
            if (rs != null) { rs.close(); }
            if (stmt != null) { stmt.close(); }
        }
        return null;
    }
}
